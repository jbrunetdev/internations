# InterNations Tech Assessment

## Requirements

- PHP 8+
- SQLite
- Composer
- Symfony CLI


## Starting the Project

You will need to run these commands in the root project
before getting started. These will make sure you have the
required dependencies, database tables and the user
we will use to test the API.

```bash
composer update
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```

Once done, run `symfony serve` to start a local web server on the
default address (`localhost:8000`).


## Testing

I have included a postman collection `InterNations.postman_collection.json`
which can be used to test all the endpoints.


### API usage

The API has authentication via a fake Bearer Token (`user=1`) to demonstrate
how the API would authenticate a user. While this isn't how a production-ready
application would implement it (you would probably use a Redis service and JWT tokens),
it can be used to check how you could use Middleware/EventSubscriber to preprocess the requests.

You can see how it works in `src/EventSubcriber/ApiSubscriber`


## Database

The database is a simple SQLite database with the basic requirements
but implementing an `N:N` relationship to manage how users get assigned to groups.
[Here you can view the database diagram](https://dbdiagram.io/d/65b63f07ac844320aedffec2)

![DB Diagram](md_images/db_diagram.png)


## Domain Model

![Domain Model](md_images/domain_model.png)


## Notes

The presented solution intentionally includes imperfections to showcase
my approach within a limited timeframe. Given more time, improvements would include:


### API

The first thing I'd do would be implement some kind of real authentication/login
and use JWT tokens to authenticate the user.

Redis would be my first choice to store the token's availability and expiration
since I could also use it to cache the API's data.

### Database

Another change would be swapping SQLite for MySQL or PostgreSQL, so it could have read/write databases,
which are more easily handled than with the former while still being able to handle multiple concurrent connections,
so when the application grows, I'd already be prepared to handle a higher volume of data.

Regarding DB security, it would also allow me to create users with limited permissions
on different tables or transactions.

### Code

While I tried to stick to Single Responsibility Principles as much as possible, I had to compromise
between developing speed and functional code, so the first thing I'd do is refactor some of the methods,
implement proper ManyToMany handling and abstracting endpoints for improved maintainability.

### Project

Consider moving to a Docker container for a smoother development process.
While Symfony lacks its own Docker image, configuring a `docker-compose` and `Dockerfile` is a subsequent step.