<?php

namespace App\Service;

use App\DTO\GroupDto;
use App\Repository\UserGroupRepository;

class GroupService
{
    private UserGroupRepository $userGroupRepository;

    public function __construct(UserGroupRepository $userGroupRepository)
    {
        $this->userGroupRepository = $userGroupRepository;
    }

    public function getGroupDtos(array $groups): array
    {
        $groupDtos = [];
        foreach ($groups as $group) {
            $groupDto = $this->createGroupDto($group);
            $groupDtos[] = $groupDto;
        }

        return $groupDtos;
    }

    private function createGroupDto($group)
    {
        // We get all results of the n:n table; this goes a bit against the
        // Single Responsibility Principle, but we will leave it to simplify the code
        $users = $this->userGroupRepository->findBy(['group_id' => $group->getId()]);
        // Pick only user ids as an array ([1, 2, 3...])
        $userIds = array_map(fn ($userGroup) => $userGroup->getUserId(), $users);

        // Creating the DTO
        $dto = new GroupDto();
        $dto->setId($group->getId());
        $dto->setName($group->getName());
        $dto->setUsers($userIds);

        return $dto;
    }
}
