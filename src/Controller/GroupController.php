<?php

namespace App\Controller;

use App\DTO\GroupDto;
use App\Entity\Group;
use App\Repository\GroupRepository;
use App\Repository\UserGroupRepository;
use App\Service\GroupService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route("/api", "api_")]
class GroupController extends AbstractController
{
    #[Route('/groups', name: 'get_groups', methods: ["GET"])]
    public function index(GroupRepository $groupRepository, GroupService $groupService): JsonResponse
    {
        $groups = $groupRepository->findAll();

        $groupDtos = $groupService->getGroupDtos($groups);

        return $this->json($groupDtos);
    }

    #[Route("/groups/{id}", "get_group", methods: ["GET"])]
    public function getGroup(int $id, GroupRepository $groupRepository, GroupService $groupService): JsonResponse
    {
        try {

        $group = $groupRepository->find($id);

        if (!$group) {
            throw new \Exception('Group not found', 404);
        }

        $groupDto = $groupService->getGroupDtos([$group]);

        return $this->json($groupDto);
        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route("/groups", "create_group", methods: ["POST"])]
    public function createGroup(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $requestBody = $request->request->all();

            $group = new Group();
            $group->setName($requestBody['name']);
            $entityManager->persist($group);
            $entityManager->flush();
            return $this->json($group);
        } catch (\ErrorException $e) {
            return $this->json(['error' => 'Wrong paramerers'], 400);
        }
    }

    #[Route("/groups/{id}", "delete_group", methods: ["DELETE"])]
    public function deleteGroup(int $id, ?Group $group, EntityManagerInterface $entityManager, UserGroupRepository $userGroupRepository): JsonResponse
    {
        try {
            if ($group === null) {
                throw new \Exception('Group not found', 404);
            }

            // We check if the group is in the n:n table. If not, it means it doesn't have any users
            $userGroups = $userGroupRepository->findBy(['group_id' => $id]);
            if (!empty($userGroups)) {
                throw new \Exception('Group still has users assigned to it', 403);
            }

            $entityManager->remove($group);
            $entityManager->flush();

            return $this->json(['message' => 'Group deleted successfully'], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }


}
