<?php

namespace App\Controller;

use App\DTO\UserGroupDto;
use App\Entity\Group;
use App\Entity\User;
use App\Entity\UserGroup;
use App\Repository\UserGroupRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/api", "api_")]
class UserController extends AbstractController
{
    #[Route('/users', name: 'get_users', methods: ["GET"])]
    public function index(UserRepository $userRepository): JsonResponse
    {
        return $this->json($userRepository->findAll());
    }

    #[Route("/users/{id}", "get_user", methods: ["GET"])]
    public function fetchUser(int $id, UserRepository $userRepository): JsonResponse
    {
        $user = $userRepository->find($id);

        // Using this method to return an error instead of an exception just to show
        // another way to do it
        if (!$user) {
            return $this->json(['error' => 'User not found'], 404);
        }
        return $this->json($userRepository->find($id));
    }

    #[Route("/users/{id}/groups", "get_user_groups", methods: ["GET"])]
    public function getUserGroups(int $id, UserRepository $userRepository, UserGroupRepository $userGroupRepository): JsonResponse
    {
        try {
            $user = $userRepository->find($id);

            if (!$user) {
                throw new \Exception('User not found', 404);
            }

            $userGroups = $userGroupRepository->findBy(['user_id' => $id]);

            $userGroupDtos = [];
            foreach ($userGroups as $userGroup) {
                $dto = new UserGroupDto();
                $dto->setUserId($userGroup->getUserId());
                $dto->setGroupId($userGroup->getGroupId());

                $userGroupDtos[] = $dto;
            }
            return $this->json($userGroupDtos);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route("/users", "create_user", methods: ["POST"])]
    public function createUser(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            $requestBody = $request->request->all();

            if ($requestBody['admin'] != 0 && $requestBody['admin'] != 1) {
                throw new \ErrorException();
            }

            $user = new User();
            // Only made users with a 'name', but you could do for example 'setEmail' or 'setLastName'
            $user->setName($requestBody['name']);
            $user->setIsAdmin($requestBody['admin']);

            $entityManager->persist($user);
            $entityManager->flush();
            return $this->json($user);
        } catch (\ErrorException $e) {
            return $this->json(['error' => 'Wrong paramerers'], 400);
        }
    }

    #[Route("/users/{id}", "delete_user", methods: ["DELETE"])]
    public function deleteUser(?User $user, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            if ($user === null) {
                throw new Exception('User not found', 404);
            }
            $entityManager->remove($user);
            $entityManager->flush();

            return $this->json(['message' => 'User deleted successfully'], 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route("/users/{id}", "update_user", methods: ["PUT"])]
    public function updateUser(?User $user, Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        try {
            if ($user === null) {
                throw new Exception('User not found', 404);
            }

            $requestBody = $request->request->all();
            if ($requestBody['is_admin'] != 0 && $requestBody['is_admin'] != 1) {
                throw new Exception('Wrong parameters', 400);
            }

            // Setting parameters dynamically without having to do them one by one.
            // While this is nice for entities with lots of attributes, it can make
            // debugging harder if something goes wrong
            foreach ($requestBody as $key => $value) {
                $set = 'set'. join('', array_map('ucfirst', explode('_', $key)));

                if (method_exists($user, $set)) {
                    $user->$set($value);
                }
            }
            $entityManager->flush();
            return $this->json(null, 200);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route("/users/{user_id}/group/{group_id}", "associate_user_group", methods: ["POST"])]
    public function associateUserGroup(int $user_id, int $group_id, EntityManagerInterface $entityManager)
    {
        try {
            $userGroup = $this->checkRelationshipEntitiesExist($entityManager, $user_id, $group_id);

            if (empty($userGroup)) {
                // Create a UserGroup entity and associate it with User and Group entities
                $user_group = new UserGroup();
                // Only made users with a 'name', but you could do for example 'setEmail' or 'setLastName'
                $user_group->setUserId($user_id);
                $user_group->setGroupId($group_id);

                // Persist and flush
                $entityManager->persist($user_group);
                $entityManager->flush();
            }
            // We always return this message; no point in complaining if the association already exists
            return $this->json(['message' => 'User added to group successfully'], 200);

        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    #[Route("/users/{user_id}/group/{group_id}", "unassociate_user_group", methods: ["DELETE"])]
    public function unassociateUserGroup(int $user_id, int $group_id, EntityManagerInterface $entityManager)
    {
        try {
            $userGroup = $this->checkRelationshipEntitiesExist($entityManager, $user_id, $group_id);

            if (!empty($userGroup)) {
                $entityManager->remove($userGroup);
                $entityManager->flush();
            }
            // We always return this message; no point in complaining if the association already exists
            return $this->json(['message' => 'User removed from group successfully'], 200);

        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode());
        }
    }

    private function checkRelationshipEntitiesExist ($entityManager, $user_id, $group_id)
    {
        // Getting the user and checking if it exists
        $userRepository = $entityManager->getRepository(User::class);
        $user = $userRepository->find($user_id);

        if ($user === null) {
            throw new Exception('User not found', 404);
        }

        // Getting the group and checking if it exists
        $groupRepository = $entityManager->getRepository(Group::class);
        $group = $groupRepository->find($group_id);

        if ($group === null) {
            throw new Exception('Group not found', 404);
        }

        $userGroupRepository = $entityManager->getRepository(UserGroup::class);
        return $userGroupRepository->findOneBy(
            ['user_id' => $user_id, 'group_id' => $group_id]
        );
    }

}
