<?php

// src/EventSubscriber/ApiPreProcessSubscriber.php

namespace App\EventSubscriber;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiSubscriber implements EventSubscriberInterface
{
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        // Check if it's an API request
        if ($this->isApiRequest($request)) {
            $this->performApiPreProcessing($request, $event);
        }
    }

    private function isApiRequest($request): bool
    {
        // We check if it's an API request by using the url
        // We could use the `Content-Type` and see if it is one we might be using (for example 'application/json')
        // In this case, we just check the url, since the `Content-Type` is not assured
        $pathInfo = $request->getPathInfo();
        return str_contains($pathInfo, '/api/' );
    }

    // Here we do all the checking and processing of the authentication
    // such as making sure the token exists and that the user is admin
    private function performApiPreProcessing($request, RequestEvent $event)
    {
        $token = $request->headers->get('Authorization');
        if (!$this->isValidToken($token)) {
            $response = new Response('Unauthorized', Response::HTTP_UNAUTHORIZED);
            $event->setResponse($response);
        }
    }

    // Here we could check for example on Redis if the token is valid, if it's expired...
    // It would be much cleaner if we just had "real" tokens and Redis to do a simple check,
    // so in this case we just kind of do a crude revision
    private function isValidToken ($token):bool
    {
        try {
            // We just leave at this point and avoid unnecessary queries if we don't have a token
            if ($token === null) {
                return false;
            }

            // If we do have a token, we now format it and check if it's a valid/existing user
            $user_id = $this->formatToken($token);
            return $this->userRepository->find($user_id)->isIsAdmin();

        } catch (\Exception $e) {
            // In this catch block, we handle invalid "strings" in case we're sent a weird token
            return false;
        }
    }

    // We don't need this if we use JWT bearer, but in this example we will need it
    private function formatToken ($token): string
    {
        return explode('=', str_replace('Bearer ', '', $token), )[1];
    }
}
